import exifread
import os
import datetime
import imghdr
from datetime import datetime, date  # , time
import re
import sys
import traceback
import logging

# -------------------------------- PROGRESS VISUALIZATON-----------------------------------------------------

total_files = None
total_dirs = None
done_files = None
done_dirs = None


def start_progress(path, dirname):
    global total_files, total_dirs, done_dirs, done_files
    print("Start counter", path, dirname)
    numdirs, numfiles = count_number_of_files_and_directories(path, dirname)
    total_files = numfiles
    total_dirs = numdirs
    done_dirs = 0
    done_files = 0

    print("Counter on ", numdirs, numfiles)


def def_update_progress_message(message):
    update_progress(0, 0, message)


def update_progress(process_dirs, process_files, status=''):
    global total_files, total_dirs, done_dirs, done_files

    done_dirs = done_dirs + process_dirs
    done_files = done_files + process_files

    count = done_dirs + done_files
    total = total_files + total_dirs
    progress(count, total, status)


def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()


def count_number_of_files_and_directories(path, dirname):
    outerdirname = os.path.join(path, dirname)
    numdirs = 0
    numfiles = 0

    for inthedir in os.listdir(outerdirname):
        subpath = os.path.join(outerdirname, inthedir)
        if os.path.isdir(subpath):
            # OPTION 1 - ITS sub directory
            numdirs = numdirs + 1

            # print("Count dir" , subpath)
            (sub_numdirs, sub_numfiles) = count_number_of_files_and_directories(outerdirname, inthedir)

            numdirs = numdirs + sub_numdirs
            numfiles = numfiles + sub_numfiles


        else:
            #  OPTION 2 - IT'S A FILE
            # print("Count file," , subpath)

            numfiles = numfiles + 1

    # print "in this dir the min max is ",(mindate,maxdate)
    if path == "." and dirname == ".":
        pass #print("skip root directory")
    else:
        pass

    return numdirs, numfiles


# -------------------------------------------------------------------------------------

def extractdatetime(path_name, filename):
    # Open image file for reading (binary mode)
    try:
        f = open(os.path.join(path_name, filename), 'rb')
        tags = exifread.process_file(f, details=False)

    except:
        print("extractdatetime: Error In opening file")
        logging.error("extractdatetime: Error In opening file"+ path_name + filename)
        return

    # Return Exif tags
    extension = os.path.splitext(os.path.join(path_name, filename))[1][1:].strip().lower()

    retrundate = ""
    if 'EXIF DateTimeOriginal' in tags:
        retrundate = tags['EXIF DateTimeOriginal']
    elif 'EXIF DateTimeDigitized' in tags:
        retrundate = tags['EXIF DateTimeDigitized']
    elif 'Image 	DateTime' in tags:
        retrundate = tags['Image DateTime']

    # also use path CHANGE? Try to read as image, and then use image changex
    elif doChanged and imghdr.what(os.path.join(path_name, filename)) != False and imghdr.what(
            os.path.join(path_name, filename)) != None:
        t = os.path.getmtime(os.path.join(path_name, filename))
        # print imghdr.what(filename)
        return datetime.fromtimestamp(t)  # direct already have a datetime object

    elif doChanged and extension in ["mp4", "mov", "avi", "amc"]:
        t = os.path.getmtime(os.path.join(path_name, filename))
        # print imghdr.what(filename)
        return datetime.fromtimestamp(t)  # direct already have a datetime object
    else:
        return

    retrundate = str(retrundate)
    try:

        thetimedate = datetime.strptime(retrundate, "%Y:%m:%d %H:%M:%S")
    except:
        logging.error('extractdatetime - cant change string date to format:'+retrundate)
        return

    # print year, month, day ,hour, minutes,seconds
    return thetimedate  # [year, month, day ,hour, minutes,seconds]


# -------------------------------- RENAMING LOGIC -----------------------------------------------------

def capture_rename_to_log(path, old, new, message):
    today = date.today()
    d = today.strftime("%Y-%m-%d %H:%M:%S")
    updatestr = d + " Rename " + old + " >> " + new + "(" + message + ")\n"

    file_rename = os.path.join(path, "rename.txt")

    # Open a file with access mode 'a'
    file_object = open(file_rename, 'a')
    file_object.write(updatestr)
    file_object.close()

def renamefile(path, filename, datum):
    if datum:
        was = filename
        new = datum.strftime("%Y-%m-%d %H.%M.%S") + " -- " + filename
        # print "compare", was[0:19], datum.strftime("%Y-%m-%d %H.%M.%S")
        if was[0:19] == datum.strftime("%Y-%m-%d %H.%M.%S"):

            def_update_progress_message("SKIP FILE RENAME - ALREADY LEADING FORMAT")
            logging.info("skip file rename, already leading" + filename)

        # print("SKIP FILE RENAME - ALREADY LEADING FORMAT")
        else:
            msg = "rename from to file " + was + new
            def_update_progress_message(msg)
            # print("rename from to file ", was, new)
            notrenamed = True
            i = 0
            while notrenamed:
                if os.path.isfile(os.path.join(path, new)):
                    new = datum.strftime("%Y-%m-%d %H.%M.%S") + " -- " + ("%i" % i) + filename
                    i = i + 1
                else:
                    try:
                        os.rename(os.path.join(path, was), os.path.join(path, new))

                        capture_rename_to_log(path, was, new, "FILE")
                        def_update_progress_message("Renamed:" + new)
                        logging.info("Renamed:"+new)
                    except Exception as e:
                        print("ERROR IN RENAME!!")
                        logging.error('renamefile - ERROR IN RENAME: WAS: ' + was + 'NEW: '+ new)
                        logging.error(e, exc_info=True)

                        traceback.print_exc()
                    notrenamed = False


    else:
        msg = "DON NOT RENAME" + filename
        logging.info( msg)
        def_update_progress_message(msg)
    # print("DON NOT RENAME", filename)


def renamedir(path, dirname, mindate, maxdate):
    # was = os.path.join(path, dirname)
    was = dirname

    norename = False

    fromdatestr = ""
    todatestr = ""
    if mindate:
        fromdatestr = mindate.strftime("%Y-%m-%d")
    if maxdate:
        todatestr = maxdate.strftime("%Y-%m-%d")

    if maxdate and mindate:
        if fromdatestr == todatestr:
            new = todatestr + " -- " + dirname
        else:
            new = fromdatestr + " - " + todatestr + " -- " + dirname
    elif maxdate:
        new = todatestr + " -- " + dirname
    elif mindate:
        new = fromdatestr + " -- " + dirname
    else:
        print("NO RENAME OF DIR POSSIBLE", was)
        logging.info("NO RENAME OF DIR POSSIBLE"+was)
        norename = True

    if was[0:10] == fromdatestr or was[0:10] == todatestr:
        print("skip directory, already containts date (one of two) in beginning")
        logging.info("skip directory, already containts date (one of two) in beginning"+was)
        norename = True

    if not norename:
        msg="rename from to dirs "+ was+ " "+new
        print(msg)
        logging.info(msg)
        try:
            os.rename(os.path.join(path, was), os.path.join(path, new))
            capture_rename_to_log(path, was, new, "DIR")

        except Exception as e:
            print("ERROR IN RENAME DIRECTORY!!", os.path.join(path, was))
            logging.error('renamedirectory - ERROR IN RENAME: WAS: ' + was + 'NEW: ' + new)
            logging.error(e, exc_info=True)


def runOnDir(path, dirname):
    mindate = None
    maxdate = None
    minnoteyetset = True
    maxnoteyetset = True

    print("------------------------------------------------------------")
    msg = "run on directory"+ path+dirname
    print(msg)
    logging.info(msg)
    # to detect skippeable files
    refiletester = re.compile(r"^\d{4}-\d{1,2}-\d{1,2}")

    outerdirname = os.path.join(path, dirname)

    for inthedir in os.listdir(outerdirname):
        subpath = os.path.join(outerdirname, inthedir)
        if inthedir[:1] == '@' and inthedir == "@eaDir":
                            # this is synology specifc - might include to overall "skip list"
            print("WE HAVE A @EA DIR SITUATION!!!")
        elif os.path.isdir(subpath):
        #if os.path.isdir(subpath) and inthedir[:1] != '@':
            # OPTION 1 - ITS sub directory
            # run recursive
            print("Found sub-dir; run recursive")
            logging.info( "Found sub-dir; run recursive")
            (subdirmindatum, subdirmaxdatum) = runOnDir(outerdirname, inthedir)

            print("found results ", subdirmindatum, subdirmaxdatum)
            logging.info("found results %s %s" % (subdirmindatum,subdirmaxdatum))

            if minnoteyetset:
                # no min here yet
                if subdirmindatum:
                    mindate = subdirmindatum
                    minnoteyetset = False
            else:
                # here is a min
                if subdirmindatum and subdirmindatum < mindate:
                    mindate = subdirmindatum

            if maxnoteyetset:
                # no max yet
                if subdirmaxdatum:
                    maxdate = subdirmaxdatum
                    maxnoteyetset = False
            else:
                # here is a max
                if subdirmaxdatum and subdirmaxdatum > maxdate:
                    maxdate = subdirmaxdatum

            update_progress(1, 0, 'processed dir')
            logging.info('processed dir')

        else:
            #  OPTION 2 - IT'S A FILE
            # TODO fast track - als dit eerste al in het format ####-##-## is dan skip?
            # ^\d{4}-\d{1,2}-\d{1,2}

            # print("looking at in the dir on file: ", inthedir)
            msg = "looking at in the dir on file: " + inthedir
            def_update_progress_message(msg)
            logging.info(msg)

            match = refiletester.match(inthedir)
            if match == None:  # if file name does not hae right naming
                def_update_progress_message("File has not right formatting" + inthedir)
                datum = runOnFile(outerdirname, inthedir)
            else:
                # seems to have a YYYY-MM-DD format or at least ####-##-##
                try:
                    datum = datetime.strptime(match.group(), "%Y-%m-%d")
                except:
                    # cant make into date
                    print("Error as still not fitting strptime format")
                    logging.error('runOnDir'+match.group())
                    datum = runOnFile(outerdirname, inthedir)

            if minnoteyetset and datum:
                # print "Set min al the way"
                mindate = datum
                minnoteyetset = False

            if maxnoteyetset and datum:
                # print "Set max al the way"
                maxdate = datum
                maxnoteyetset = False

            if datum:
                if datum < mindate:
                    mindate = datum
                if datum > maxdate:
                    maxdate = datum

            update_progress(0, 1, 'processed file')

    # print "in this dir the min max is ",(mindate,maxdate)
    if path == "." and dirname == ".":
        print("skip root directory")
        logging.info("skip root directory")
    else:
        renamedir(path, dirname, mindate, maxdate)

    # return tuple with start and enddate
    return mindate, maxdate


def runOnFile(path, filename):
    logging.info('run on file' + path + filename)

    # find date of the file
    dateoffile = extractdatetime(path, filename)
    # rename to right format
    renamefile(path, filename, dateoffile)
    # return date in recursve
    return dateoffile


if __name__ == '__main__':
    doChanged = True
    logging.basicConfig(filename='imagerenamer.log', level=logging.INFO)
    #logging.basicConfig(filename='imagerenamer.log', level=logging.DEBUG)

    logging.info('START')

    #TODO paramaterize
    path = "."
    dir = "."

    # start & setup progress bar
    start_progress(path, dir)

    # run on the directory
    runOnDir(path, dir)

# TODO: improve output of text on screen/log
# TODO: consider what to do with "too long file names"
