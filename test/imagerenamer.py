import exifread
import glob
import os
import datetime
import imghdr
from datetime import datetime, date, time

def extractdatetime(path_name, filename):
    # Open image file for reading (binary mode)
    try:
        f = open(os.path.join(path_name,filename), 'rb')
    except:
        print "ERRRRRRROR In opening file"
        return
    
    # Return Exif tags
    tags = exifread.process_file(f)
    #for tag in tags.keys():
    #    if tag not in ('JPEGThumbnail', 'TIFFThumbnail', 'Filename', 'EXIF MakerNote'):
    #print "Key: %s, value %s" % (tag, tags[tag])

    retrundate = ""
    if 'EXIF DateTimeOriginal' in tags:
        retrundate = tags['EXIF DateTimeOriginal']
    elif 'EXIF DateTimeDigitized' in tags:
        retrundate = tags['EXIF DateTimeDigitized']
    elif 'Image 	DateTime' in tags:
        retrundate = tags['Image DateTime']
    elif doChanged and imghdr.what(filename) != False and imghdr.what(filename) != None:
        t = os.path.getmtime(filename)
        print imghdr.what(filename)
        return datetime.fromtimestamp(t) #direct already have a datetime object
    else:
        return

    retrundate = str(retrundate)
    try:

        thetimedate =  datetime.strptime(retrundate, "%Y:%m:%d %H:%M:%S")
    except:
	    return
	#print year, month, day ,hour, minutes,seconds
    return thetimedate #[year, month, day ,hour, minutes,seconds]


def renamefile(path,filename, datum):
    if datum:
        was = filename
        new = datum.strftime("%Y-%m-%d %H.%M.%S") +" -- " + filename
        #print "compare", was[0:19], datum.strftime("%Y-%m-%d %H.%M.%S")
        if was[0:19] == datum.strftime("%Y-%m-%d %H.%M.%S"):
            print "SKIP FILE RENAME - ALREADY LEADING FORMAT"
        else:
            print "rename from to file ", was, new
            notrenamed = True
            i = 0
            while notrenamed:
                if os.path.isfile(os.path.join(path,new)):
                    new = datum.strftime("%Y-%m-%d %H.%M.%S") + " -- " + ("%i" % i) + filename
                    i = i + 1
                else:
                    os.rename(os.path.join(path, was), os.path.join(path,new))
                    notrenamed = False
    
    
    else:
        print "DON NOT RENAME", filename

def renamedir(path,dirname,mindate, maxdate):

#was = os.path.join(path, dirname)
    was = dirname
    
    norename= False
    
    fromdatestr=""
    todatestr=""
    if mindate:
        fromdatestr = mindate.strftime("%Y-%m-%d")
    if maxdate:
        todatestr = maxdate.strftime("%Y-%m-%d")

    if maxdate and mindate:
        if fromdatestr == todatestr:
            new =  todatestr + " -- " + dirname
        else:
            new = fromdatestr + " - " + todatestr + " -- " + dirname
    elif maxdate:
        new =  todatestr + " -- " + dirname
    elif mindate:
        new =  fromdatestr + " -- " + dirname
    else:
        print "NO RENAME OF DIR POSSIBLE", was
        norename = True

    if was[0:10] == fromdatestr or was[0:10] == todatestr:
        print "skip directory, already containts date (one of two) in beginning"
        norename = True

    if not norename:
        print "rename from to dirs ", was, new
        os.rename(os.path.join(path, was), os.path.join(path,new))



def runOnDir(path,dirname):
    mindate = None
    maxdate = None
    minnoteyetset = True
    maxnoteyetset = True

    print ""
    #print "run on dir", path, dirname
    
    outerdirname = os.path.join(path, dirname)
    # return tuple with start and enddate
    for inthedir in os.listdir(outerdirname):
        subpath = os.path.join(outerdirname,inthedir)
        if os.path.isdir(subpath):
            # sub directory
            #(submindate,submaxdata) = runOnDir (subpath)
            #print("run on subdate" , subpath, mindate, maxdate)
            (subdirmindatum, subdirmaxdatum) = runOnDir(outerdirname,inthedir)
            #print "outcome of recursion - dirs and dates", outerdirname, inthedir, subdirmindatum, subdirmaxdatum
            
            if minnoteyetset == True:
                # no min here yet
                if subdirmindatum:
                    mindate = subdirmindatum
                    minnoteyetset = False
            else:
                # here is a min
                if subdirmindatum and subdirmindatum < mindate:
                    mindate = subdirmindatum
            
            if maxnoteyetset == True:
                # no max yet
                if subdirmaxdatum:
                    maxdate = subdirmaxdatum
                    maxnoteyetset = False
            else:
                # here is a max
                if subdirmaxdatum and subdirmaxdatum > maxdate:
                    maxdate = subdirmaxdatum
                
                """
            if mindate == None or  (subdirmindatum and subdirmindatum < mindate):
                mindate = subdirmindatum
                minnoteyetset = False
            if maxdate == None or (subdirmaxdatum and subdirmaxdatum < maxdate):
                maxdate = subdirmaxdatum
                maxnoteyetset = False
                """
            #print("after run on subdate" , subpath, mindate, maxdate)
    
        else:
            #print "incoming file",  inthedir, mindate, maxdate
            # file. zou ik nog proberen ext = jpg?
            datum = runOnFile(outerdirname, inthedir)
            #print mindate, datum
            if minnoteyetset and datum:
                #print "Set min al the way"
                mindate = datum
                minnoteyetset = False
            
            if maxnoteyetset and datum:
                #print "Set max al the way"
                maxdate = datum
                maxnoteyetset = False
            
            if datum:
                if datum < mindate:
                    mindate = datum
                if datum > maxdate:
                    maxdate = datum
            #print "per file", inthedir, datum, mindate, maxdate


    #print "in this dir the min max is ",(mindate,maxdate)
    if path == "." and dirname == ".":
        print "skip root directory"
    else:
        renamedir(path, dirname,mindate,maxdate)
    return (mindate,maxdate)

def runOnFile(path,filename):
    #print("do op file %s || %s" % (path, filename))
    dateoffile=  extractdatetime(path,filename)
    #dateOfFile = ""
    renamefile(path, filename,dateoffile)
    return dateoffile

if __name__ == '__main__':
    doChanged = True
    # TBD een "switch" of je ook  created als alternative pakt
    runOnDir(".",".")

